/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

%{
#include <stdio.h>
#include <stdlib.h>
#include "evaluadorData.h"

int yylex(void);
void yyerror (char const *);
%}

%union {
  char *vtext;
  int  vnumber;
  struct Conjunto* vconjunto;
  struct LConjuntos* vlistaConjuntos;
  struct Asignacion* vasignacion;
  struct LAsignaciones* vlistaAsignaciones;
  struct node* vnode;
  }

%token CONJUNTO ABRELLAVE CIERRALLAVE ABREPARENTESIS CIERRAPARENTESIS PUNTOYCOMA
%token <vtext> ID
%token <vnumber> NUMBER
%right ASIGNACION
%left SUMA RESTA
%left MULTIPLICACION DIVISION
%nonassoc NEG       /* negation --unary minus*/
%start abssyn
%type <vlistaConjuntos> abssyn
%type <vlistaConjuntos> evaluadorcfg
%type <vconjunto> conjunto
%type <vlistaAsignaciones> asignaciones
%type <vasignacion> asignacion
%type <vnode> expresion
%%

abssyn:		evaluadorcfg {yylval.vlistaConjuntos = $1;}
	;

evaluadorcfg:	conjunto evaluadorcfg  { $$ = addListaConjuntos($2, $1);                       }
	|	conjunto               { $$ = addListaConjuntos((struct LConjuntos*)NULL, $1); }
	;

conjunto:	CONJUNTO NUMBER ABRELLAVE asignaciones CIERRALLAVE { $$ = createConjunto($2, $4); }
        ;

asignaciones:	asignacion asignaciones  {$$ = addListaAsignaciones($2, $1);                           }
	|	asignacion               { $$ = addListaAsignaciones((struct LAsignaciones*)NULL, $1); }
	;

asignacion:	ID ASIGNACION expresion PUNTOYCOMA { $$ = createAsignacion($1, $3); }
	;

expresion:	ID                              { $$ = newIDN($1);     }
	|	NUMBER                          { $$ = newNumber($1);  }
	|	expresion SUMA expresion         { $$ = newSum($1, $3); }
	|	expresion RESTA expresion         { $$ = newRes($1, $3); }
	|	expresion MULTIPLICACION expresion         { $$ = newMul($1, $3); }   
	|	expresion DIVISION expresion         { $$ = newDiv($1, $3); }
	|	RESTA expresion %prec NEG         { $$ = newUmin($2);    }
	|	ABREPARENTESIS expresion CIERRAPARENTESIS   { $$ = $2;             }
	;

%%
