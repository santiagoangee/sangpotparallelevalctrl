/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#include "evaluadorparser.h"
#include "evaluadorlexer.h"
#include "evaluadorData.h"
#include "execParserEvaluador.h"

extern int yyparse();

/*Ejecuta el parser del evaluador al recibir un archivo en el que identifica y retorna los conjuntos*/
plConjuntos_t
execParserEvaluador(FILE* inFile) {
    
    yyin = inFile;
    
    if(!yyparse()) {
	return yylval.vlistaConjuntos;
    }
    return NULL;
}

void yyerror(const char* msg) {
    fprintf(stderr, "evaluadorparser:%s at %d  in '%s'\n", msg, yylineno, yytext);
}
