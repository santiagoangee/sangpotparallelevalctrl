/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#include <stdlib.h>
#include "entornoparser.h"
#include "entornolexer.h"
#include "entorno.h"
#include "execParserEntorno.h"

extern int entornoparse();
extern void entorno_delete_buffer(YY_BUFFER_STATE);

int realStrLen(const char *s) {
  int len = 0;
  while(*s) {
    len++;
    s++;
  }
  return len;
}
/*Ejecuta el parser del entorno al recibir una cadena de caracteres a evaluar, devuelve el entorno que puede ser vacio o por el contrario devuelve un mensaje de error y retorna un entorno vacio*/
pvariablesHash_t
execParserEntorno(const char *entorno) {
  
  fprintf(stderr, "execParserEntorno.c: entorno == '%s'\n", entorno);
  if (entorno) {
    YY_BUFFER_STATE ebf;
    ebf = entorno_scan_string(entorno);
    
    fprintf(stderr,"execParserEntorno.c: entorno length: %d\n", realStrLen(entorno));
    if (!entornoparse()) {
      
      entorno_delete_buffer(ebf);
      fprintf(stderr, "execParserEntorno.c: parsing en execParserEntorno funciona!\n");
      return entornolval.vars;
      
    }
    
    entorno_delete_buffer(ebf);
    fprintf(stderr, "execParserEntorno.c: entorno incorrecto!\n");
    return NULL;
    
  } else {
    fprintf(stderr, "execParserEntorno.c: environment variable ENTORNO does not exists\n");
    exit(1);
  }
  
}

void entornoerror(const char *msg) {
  fprintf(stderr, "execParserEntorno.c: :%s at %d in '%s'\n", msg, entornolineno, entornotext);
}
