/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "entorno.h"
#include "evaluadorData.h"


/* 
 * La evaluacion se realiza mediante estructuras de arboles.
 * Definiciones correspondientes para el árbol de operaciones y su evaluación.
 * Ver archivo evaluadorData.h para documentacion mas profunda
 */

char*
newID(char* oldID) {
    char* ret;
    char* pos;
    pos = ret = malloc(strlen(oldID) + 1);
    if (ret) {
	while ((*pos++ = *oldID++)) {
	}
    }
    return ret;
}

PNode
newNode() {
    PNode ret = (PNode) malloc(sizeof(Node));
    ret->leftChild = ret->rightChild = NULL;
    return ret;
}

PNode
newIDN(char* id) {
    PNode ret;

    ret = newNode();
    ret->tagInfo = T_ID;
    ret->info.id = id;
    return ret;
}

PNode
newNumber(int value) {
    PNode ret;

    ret = newNode();
    ret->tagInfo = T_NUMBER;
    ret->info.numero = value;
    return ret;
}

PNode
newSum(PNode left, PNode right) {
    PNode ret;
    ret = newNode();
    ret->tagInfo = T_OPER;
    ret->info.oper = O_SUM;
    ret->leftChild = left;
    ret->rightChild = right;

    return ret;
}

PNode
newRes(PNode left, PNode right) {
    PNode ret;
    ret = newNode();
    ret->tagInfo = T_OPER;
    ret->info.oper = O_RES;
    ret->leftChild = left;
    ret->rightChild = right;

    return ret;
}

PNode
newMul(PNode left, PNode right) {
    PNode ret;
    ret = newNode();
    ret->tagInfo = T_OPER;
    ret->info.oper = O_MUL;
    ret->leftChild = left;
    ret->rightChild = right;

    return ret;
}

PNode
newDiv(PNode left, PNode right) {
    PNode ret;
    ret = newNode();
    ret->tagInfo = T_OPER;
    ret->info.oper = O_DIV;
    ret->leftChild = left;
    ret->rightChild = right;

    return ret;
}

PNode
newUmin(PNode right) {
    PNode ret;
    ret = newNode();
    ret->tagInfo = T_OPER;
    ret->info.oper = O_UMIN;
    ret->leftChild = NULL;
    ret->rightChild = right;

    return ret;
}

int
eval(PNode root, pvariablesHash_t entorno) {
    switch(root->tagInfo) {
    case T_OPER:
	switch(root->info.oper) {
	case O_SUM:
	  return eval(root->leftChild, entorno) + eval(root->rightChild, entorno);
	    break;

	case O_RES:
	  return eval(root->leftChild, entorno) - eval(root->rightChild, entorno);
	    break;

	case O_MUL:
	  return eval(root->leftChild, entorno) * eval(root->rightChild, entorno);
	    break;

	case O_DIV:
	  return eval(root->leftChild, entorno) / eval(root->rightChild, entorno);
	    break;

	case O_UMIN:
	  return -(eval(root->rightChild, entorno));
	    break;
	}
	break;

    case T_NUMBER:
	return root->info.numero;
	break;

    case T_ID:
	return find_variable_value(entorno, root->info.id);
	break;
    }
    return 0;
}

/*
 * Definiciones que corresponden a la lista de asignaciones y la lista de conjuntos
 */

pAsignacion_t
createAsignacion(char *id, PNode node) {
    pAsignacion_t pa;
    pa = (pAsignacion_t)malloc(sizeof(Asignacion_t));
    pa->id = id;
    pa->assignationTree = node;
    return pa;
}


plAsignaciones_t
newLAsignaciones() {
    plAsignaciones_t la = (plAsignaciones_t)malloc(sizeof(lAsignaciones_t));
    la->pa = (pAsignacion_t)NULL;
    la->next = (plAsignaciones_t)NULL;
    return la;
}

plAsignaciones_t addListaAsignaciones(plAsignaciones_t head, pAsignacion_t pa)
{
    plAsignaciones_t nHead = newLAsignaciones();
    nHead->pa = pa;
    nHead->next = head;
    return nHead;
}

pvariablesHash_t
evalLAsignaciones(plAsignaciones_t list, pvariablesHash_t entorno) { 
    while (list) {
	pAsignacion_t pa = list->pa;

	int value = eval(pa->assignationTree, entorno);
	pvariable_t var = create_variable(pa->id, value);
	entorno = load_variable(var, entorno);
	list = list->next;
    }
    return entorno;
}

pConjunto_t
createConjunto(int id, plAsignaciones_t asignaciones) {
    pConjunto_t pc;
    pc = (pConjunto_t)malloc(sizeof(Conjunto_t));
    pc->id = id;
    pc->asignaciones = asignaciones;
    return pc;
}


plConjuntos_t
newLConjuntos() {
    plConjuntos_t lc = (plConjuntos_t)malloc(sizeof(lConjuntos_t));
    lc->pc = (pConjunto_t)NULL;
    lc->next = (plConjuntos_t)NULL;
    return lc;
}

plConjuntos_t addListaConjuntos(plConjuntos_t head, pConjunto_t pc)
{
    plConjuntos_t nHead = newLConjuntos();
    nHead->pc = pc;
    nHead->next = head;
    return nHead;
}

pvariablesHash_t
evalConjunto(int numero, plConjuntos_t list, pvariablesHash_t entorno) {
    
    while (list)
	{
	    pConjunto_t pc = list->pc;
	    
	    if (pc->id == numero) {
		entorno = evalLAsignaciones(pc->asignaciones, entorno);
	    }
	    list = list->next;
	}
    return entorno;
}

void showLConjuntos(plConjuntos_t list, pvariablesHash_t entorno) { 
    while (list) {
	pConjunto_t pc = list->pc;
	fprintf(stderr, "Conjunto %d {\n", pc->id);
	evalLAsignaciones(list->pc->asignaciones, entorno);
	fprintf(stderr, "}\n");
	list = list->next;
    }
}

void
deleteAssignationTree(PNode node)
{
  if (!node) {
    fprintf(stderr, "evaluadorData.c: deleteAssignationTree: 'node == NULL'.\n");
  } else {
    if (node->leftChild) {
      deleteAssignationTree(node->leftChild);
    }
    if (node->rightChild) {
      deleteAssignationTree(node->rightChild);
    }
    free(node->info.id);
    free(node);
    node = NULL;
  }
}

int
deleteAsignacion(pAsignacion_t pa)
{
  if (!pa) {
    fprintf(stderr, "evaluadorData.c: deleteAsignacion: 'asignaciones == NULL'.\n");
  } else if (!pa->id || !pa->assignationTree) {
    fprintf(stderr, 
	    "evaluadorData.c: deleteAsignacion: Error: asignacion without 'id' or 'assignationTree'. ");
    fprintf(stderr, "Default: 'pa' was deleted\n");
    if (pa->id) {
      free(pa->id);
      pa->id = NULL;
    }
    if (pa->assignationTree) {
      deleteAssignationTree(pa->assignationTree);
      pa->assignationTree = NULL;
    }
    free(pa);
    pa = NULL;
    return -1;
  } else {
    free(pa->id);
    deleteAssignationTree(pa->assignationTree);
    pa->assignationTree = NULL;
    free(pa);
    pa = NULL;
  }
  return 0;
}

void
deleteLAsignaciones(plAsignaciones_t asignaciones)
{
  if (!asignaciones) {
    fprintf(stderr, "evaluadorData.c: deleteLAsignaciones: 'asignaciones == NULL'.\n");
  } else if (asignaciones->next) {
    deleteLAsignaciones(asignaciones->next);
    deleteAsignacion(asignaciones->pa);
    free(asignaciones);
    asignaciones = NULL;
  } else {
    deleteAsignacion(asignaciones->pa);
    free(asignaciones);
    asignaciones = NULL;
  }
}

int
deleteConjunto(pConjunto_t pc)
{
  if (!pc) {
    fprintf(stderr, "evaluadorData.c: deleteConjunto: 'pc == NULL'.\n");
  }
  else if (!pc->asignaciones) { /* Error */
    fprintf(stderr, 
	    "evaluadorData.c: deleteConjunto: Error: 'conjunto' without 'asignaciones'. ");
    fprintf(stderr, "Default: conjunto was deleted\n");
    free(pc);
    pc = NULL;
    return -1;
  } else {
    deleteLAsignaciones(pc->asignaciones);
    free(pc);
    pc = NULL;
  }
  return 0;
}

void
deleteLConjuntos(plConjuntos_t list)
{
  if (!list) {
    fprintf(stderr, "evaluadorData.c: deleteLConjuntos: 'list == NULL'.\n");
  } else if (list->next) {
    deleteLConjuntos(list->next);
    deleteConjunto(list->pc);
    free(list);
    list = NULL;
  } else {
    deleteConjunto(list->pc);
    free(list);
    list = NULL;
  }
}
