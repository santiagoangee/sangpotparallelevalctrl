/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#ifndef _ENTORNO_H
#define _ENTORNO_H

/*Esta estructura corresponde a variable que contiene una cadena de caracteres identificando el nombre y un valor*/
struct variable {
  char *name;    /* key */
  int value;
};
/*Definen los tipos de estructuras, que evitan el tener que volver a declarar tipos de variables*/
typedef struct variable variable_t;
typedef struct variable* pvariable_t;
typedef struct variablesHash variablesHash_t;
typedef struct variablesHash* pvariablesHash_t;

/*Esta estructura corresponde a variableHash que contiene un apuntador a variable y un apuntador a variableHash*/
struct variablesHash {
    pvariable_t pv;
    pvariablesHash_t next;
};

/*crea una copia de un string ingresado en una nueva posición de memoria*/
char* newCopyString(char *n);
/*Función que crea una variable*/ 
pvariable_t create_variable(char *varname, int value);
/*Función que encuentra una variable*/
pvariable_t findVariable(char *key, pvariablesHash_t vartable);
/*Función que agrega una variable, en caso de que no exista, y si existe modifica la variable con el valor ingresado*/
pvariablesHash_t load_variable(pvariable_t var, pvariablesHash_t vars);
/*Función que encuentra el valor de una variable*/
int find_variable_value(pvariablesHash_t vars, char *varname);
/*Función que retorna el entorno*/
void get_entorno(pvariablesHash_t vars, char *buffer, int bufferLength);
/*Función que borra todas las variables registradas*/
void delete_all(pvariablesHash_t vars);
#endif
