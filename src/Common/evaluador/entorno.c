/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "entorno.h"

#define TRUE  1
#define FALSE 0

/*crea una copia de un string ingresado en una nueva posición de memoria*/
char*
newCopyString(char *n){
  char *p;
  int longitud;

  longitud = strlen(n);
  p = (char *) malloc((longitud + 1)*sizeof(char));
  strcpy(p,n);
  return p;
}
/*Crea una variable*/ 
pvariable_t
create_variable(char *varname, int value)
{
  pvariable_t var = (pvariable_t)malloc(sizeof(variable_t));
  var->name = newCopyString(varname);
  var->value = value;
  return var;
}
/*Crea una nueva VariableHash*/
pvariablesHash_t
newVariablesHash() {
 pvariablesHash_t vars = (pvariablesHash_t)malloc(sizeof(variablesHash_t));
 vars->pv = (pvariable_t)NULL;
 vars->next = (pvariablesHash_t)NULL;
 return vars;
}
/*Encuentra una variable que se haya ingresado*/
pvariable_t
findVariable(char *key, pvariablesHash_t vartable)
{
  pvariablesHash_t current = vartable;
  pvariable_t var;
  if (current) {
    do {
      var = current->pv;
      if (strcmp(key, var->name) == 0) {
	//printf("entorno.c: keyfound: %s=%d\n",var->name, var->value);
	return var;
      }
      current = current->next;
    } while (current);
  }
  return (pvariable_t)NULL;
}
/*Agrega una variable, en caso de que no exista, y si existe la modifica con el valor ingresado*/
pvariablesHash_t
load_variable(pvariable_t var, pvariablesHash_t vars)
{
  pvariable_t match;
  if (!(match = findVariable(var->name, vars))) {
    pvariablesHash_t nHead = newVariablesHash();
    nHead->pv = var;
    nHead->next = vars;
    return nHead;
  } else {
    int temp = var->value;
    match->value = temp;
    free(var);
    var = NULL;
    return vars;
  }
}
/*Borra todas las variables registradas*/
void delete_all(pvariablesHash_t vars)
{
  if (!vars) {
    fprintf(stderr, "entorno.c: delete_all: Nothing to delete.\n");
  }
  else if (vars->next) {
    delete_all(vars->next);
    free(vars->pv->name);
    free(vars->pv);
    free(vars);
    vars = (pvariablesHash_t)NULL;
  } else {
    free(vars->pv->name);
    free(vars->pv);
    free(vars);
    vars = (pvariablesHash_t)NULL;
  }
}

/*void delete_all(pvariablesHash_t vars) {
  
  pvariablesHash_t next;
  if (vars) {
    do {
      next = vars->next;
      free(vars->pv->name);
      vars->pv->name = NULL;
      free(vars->pv);
      vars->pv = NULL;
      free(vars);
      vars = NULL;
      vars = next;
    } while (vars);
  }
  
  }*/

/*Encuentra el valor de una variable*/
int find_variable_value(pvariablesHash_t vars, char *varname)
{
  pvariable_t result = findVariable(varname, vars);
  int value;
  if (result) {
    value = result->value;
    return value;
  }
  return 0;
}

/*Retorna una cadena de caracteres con el entorno correspondiente*/
/* Must be initialized with a buffer of MAX_ENTORNO_SZ length */
void
get_entorno(pvariablesHash_t vars, char *buffer, int bufferLength)
{
  pvariablesHash_t tmp = vars;
  pvariable_t current;
  memset(buffer, 0, bufferLength);
  strcpy(buffer, "{");
  if (tmp) {
    do {
      current = tmp->pv;
      char num[11];
      memset(num, 0, 11);
      strcat(buffer, current->name);
      strcat(buffer, "=");
      sprintf(num, "%d", current->value);
      strcat(buffer, num);
      strcat(buffer, ";");
      tmp = tmp->next;
    } while (tmp);
  }
  strcat(buffer, "}");
  return;
}
