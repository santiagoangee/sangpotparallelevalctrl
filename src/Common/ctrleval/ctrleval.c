#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "ctrlEvalData.h"
#include "execParserCtrlEval.h"

#define MAX_ENTORNO_SZ 1000
#define MAX_PATH_SZ 300
#define MAX_PROCESS_SZ 100

void
usage(char *progname) {
  fprintf(stderr, "Usage %s\n", progname);
  exit(1);
}

int getNumConjuntos(const char *cfgFile)
{
  FILE *inFile   = fopen(cfgFile, "r");
 
  if (!inFile) {
    fprintf(stderr,"ctrleval.c: getNumColnjuntos: no se pudo abrir 'inFile'\n");
  }

  int numConjuntos = 0;

  if ((numConjuntos = execParserCtrlEval(inFile))) {

    
    fprintf(stdout, "¡Parser archivo de configuración funcionó!\n");
    printf("Número de conjuntos = %d\n", numConjuntos);
  }
  else {
    
    fprintf(stderr, "¡Parser archivo de configuración no funcionó!\n");
  }

  fclose(inFile);
  
  return numConjuntos;
}

/* Esta función está pensada para convertir de int a char * un número de no más de 4 dígitos */
char * 
itoa(int number)
{
  char *result = (char*)malloc(5*sizeof(char));
  memset(result, 0, 5);
  snprintf(result, 5, "%d", number);
  return result;
}

void
initPipe(int **matrix, int size)
{
  int i;
  for (i = 0; i < size; ++i) {
    matrix[i] = (int*)malloc(2*sizeof(int));
  }
}


int 
main(int argc, char *argv[]) {

  if (argc != 1) {
    usage(argv[0]);
  }
  
  char * entorno = (char*)malloc(MAX_ENTORNO_SZ*sizeof(char));
  memset(entorno, 0, MAX_ENTORNO_SZ);
  ssize_t tam;
  
  int num_conjuntos = getNumConjuntos("../evaluador/ctrleval.cfg");
  int i;
  // int pipe_in[MAX_PROCESS_SZ][2];
  //int pipe_out[MAX_PROCESS_SZ][2];
  //pid_t ptChild[MAX_PROCESS_SZ];
  int **pipe_in = (int**)malloc(num_conjuntos*sizeof(int*));
  int **pipe_out = (int**)malloc(num_conjuntos*sizeof(int*));
  initPipe(pipe_in, num_conjuntos);
  initPipe(pipe_out, num_conjuntos);
  pid_t *ptChild = (int*)malloc(num_conjuntos*sizeof(int));
  memset(entorno, 0, MAX_ENTORNO_SZ);

  char *progpath = (char*)malloc(MAX_PATH_SZ*sizeof(char));
  char *filepath = (char*)malloc(MAX_PATH_SZ*sizeof(char));
  memset(progpath, 0, MAX_PATH_SZ);
  strcat(progpath, "../evaluador/evaluador");
  memset(filepath, 0, MAX_PATH_SZ);

  char *dirdetrabajo = getenv("DIRDETRABAJO");
  
  strcat(filepath, dirdetrabajo);
  strcat(filepath, "/");

  char *ficherocfg = getenv("FICHEROCFG");
  strcat(filepath, ficherocfg);
  
  for (i = 0; i < num_conjuntos; ++i) {
    if (pipe(pipe_in[i]) == -1) { 
      
      /* Algo salió mal */
      fprintf(stderr, "Error: Debido a: %d %s, en pipe[%d]\n", errno, strerror(errno), i);
    }
    
    if (pipe(pipe_out[i]) == -1) {
      fprintf(stderr, "Error: Debido a: %d %s, en pipe[%d]\n", errno, strerror(errno), i);
    }
    
    /* Crea el hijo */
    if ((ptChild[i] = fork()) == 0) {
      
      /*close unneeded */
      close(pipe_in[i][1]);
      close(pipe_out[i][0]);
      
      /* Make pipes the stdin and stdout */
      close(0);
      dup2(pipe_in[i][0], 0);
      close(pipe_in[i][0]); 
      
      close(1);
      dup2(pipe_out[i][1], 1);
      close(pipe_out[i][1]);
      
      /*run the child process */
      
      execl(progpath, "evaluador",
	    filepath, itoa(i+1), (char *) NULL);
      /* Algo malo ocurrió */
      fprintf(stderr, "Error: este proceso nunca debería alcanzar este punto\n");
      exit(2);
    } else /* Parent */ {
      
      /* close unneeded */
      close(pipe_in[i][0]);
      close(pipe_out[i][1]);
      
    }
  }  
  
  while (1) {
    if ((tam = read(0, entorno, MAX_ENTORNO_SZ)) >= 0) {
      entorno[tam] = '\0';
      for(i = 0; i < num_conjuntos; ++i) {
	size_t tam;
	write(pipe_in[i][1], entorno, strlen(entorno));
	memset(entorno, 0, MAX_ENTORNO_SZ);
	if ((tam = read(pipe_out[i][0], entorno, MAX_ENTORNO_SZ)) >= 0) {
	  
	  entorno[tam] = '\0';
	  fprintf(stderr, "ctrleval.c: entorno == '%s'\n", entorno);
	  //sleep(3);
	}
	else { 
	  fprintf(stderr, "ctrleval.c: error = %d \n", errno);
	  exit(1);
	}
      }
      write(1, entorno, strlen(entorno));
      memset(entorno, 0, MAX_ENTORNO_SZ);
    } else {
      fprintf(stderr, "ctrleval.c: Error: no se pudo leer el entorno");
    }
  }
  
  return 0;
    
}
