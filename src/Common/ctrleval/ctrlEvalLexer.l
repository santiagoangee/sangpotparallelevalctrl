%{
#include "ctrlEvalData.h"
#include "ctrlEvalParser.h"
%}

%option noyywrap

%%
[\t \n]* ;
Conjunto                             { return CONJUNTO;         }
[A-Za-z][A-Za-z0-9]*                 { return ID;               }
\{                                   { return ABRELLAVE;        }
\}                                   { return CIERRALLAVE;      }
\(                                   { return ABREPARENTESIS;   }
\)                                   { return CIERRAPARENTESIS; }
=                                   { return ASIGNACION;       }
\+                                   { return SUMA;             }
\*                                   { return MULTIPLICACION;   }
\-                                   { return RESTA;            }
\/                                   { return DIVISION;         }
;                                    { return PUNTOYCOMA;       }
([1-9][0-9]*|0)                      { yylval.vnumber = atoi(yytext);
                                       return NUMBER;
                                     }
<<EOF>>                              { yyterminate();           }
%%


