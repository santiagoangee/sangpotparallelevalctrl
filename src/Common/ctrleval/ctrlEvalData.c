#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ctrlEvalData.h"

char*
newCopyString(char n[]){
  char *p;
  int longitud;

  longitud = strlen(n);
  p = (char *) malloc(longitud + 1);
  strcpy(p,n);
  return p;
}

pConjunto_t
createConjunto(int id) {
    pConjunto_t pc;
    pc = (pConjunto_t)malloc(sizeof(Conjunto_t));
    pc->id = id;
    return pc;
}


plConjuntos_t
newLConjuntos() {
    return (plConjuntos_t)malloc(sizeof(lConjuntos_t));
}

plConjuntos_t addListaConjuntos(plConjuntos_t head, pConjunto_t pc)
{
    plConjuntos_t nHead = newLConjuntos();
    nHead->pc = pc;
    nHead->next = head;
    return nHead;
}

int lengthLConjuntos(plConjuntos_t list)
{
    int length = 0;

    if (list)
    {
	do {
	    printf("current length is: %d\n", length);
	    length++;
	    list = list->next;
	} while (list);
	
	return length;
    }
    
    return 0;
}
