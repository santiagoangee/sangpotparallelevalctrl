#include "ctrlEvalParser.h"
#include "ctrlEvalLexer.h"
#include "ctrlEvalData.h"
#include "execParserCtrlEval.h"

extern int yyparse();

int
execParserCtrlEval(FILE *inFile) {

    yyin = inFile;
    
    int length = 0;

    if (!yyparse()) {
	plConjuntos_t conjuntos = yylval.vlconjuntos;
	
	length = lengthLConjuntos(conjuntos);
	
	return length;
    }

    return (int)NULL;
    
}

void yyerror(const char *msg) {
    fprintf(stderr, "%s at %d  in '%s'\n", msg, yylineno, yytext);
}
