%{

/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include "ctrlsysData.h"
#include "ctrlsysParser.h"
%}

%option noyywrap

%%
[\t \n]* ;
Evaluador                          { return EVALUADOR;                       }
\.(\/[A-Za-z][A-Za-z0-9]*)+        { yylval.vtext = newCopyString(yytext);
                                     return PATH;
                                   }
[A-Za-z][A-Za-z0-9]*\.cfg          { yylval.vtext = newCopyString(yytext);
                                     return EVALCFG;
                                   }
[A-Za-z][A-Za-z0-9]*               { yylval.vtext = newCopyString(yytext);
                                     return ID;
                                   }
\{                                 { return ABRELLAVE;                       }
\}                                 { return CIERRALLAVE;                     }
<<EOF>>                            { yyterminate(); }
%%


